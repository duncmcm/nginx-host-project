import { NginxHostProjectPage } from './app.po';

describe('nginx-host-project App', () => {
  let page: NginxHostProjectPage;

  beforeEach(() => {
    page = new NginxHostProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
